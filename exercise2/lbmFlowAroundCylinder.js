// Define domain size.
const nx = 400; // Width of the domain
const ny = 100; // Height of the domain
const nDir = 9; // Number of directions in D2Q9 model.

// Define cylinder parameters.
const radius = ny/10;
const xCenter = Math.floor(nx / 5);
const yCenter = Math.floor(ny / 2 - 3);

// Flow properties.
const rho0 = 1.0; // Reference density
const uLB = 0.04; // Initial and inlet horizontal velocity (Lattice units).
const Re = 500;   // Reynolds number

// Compute viscosity and relaxation time.
const nuLB = uLB * radius / Re; // Kinematic viscosity in lattice units.
const tau = 3 * nuLB + 0.5;     // Relaxation time.

// Setup the canvas.
const canvas = document.getElementById("lbmCanvas");
const ctx = canvas.getContext("2d");
canvas.width = nx;
canvas.height = ny;

// Define D2Q9 model parameters.
const cx = [1, 1,  1, 0, 0,  0, -1, -1, -1]; // x-components of velocity set.
const cy = [1, 0, -1, 1, 0, -1,  1,  0, -1]; // y-components of velocity set.
const w  = [1/36, 1/9, 1/36, 1/9, 4/9, 1/9, 1/36, 1/9, 1/36]; // Weights for each direction.

// Allocate memory for populations and velocity
let fin = new Array(nx).fill(0).map(() => new Array(ny).fill(0).map(() => new Array(nDir).fill(0)));
let fout = new Array(nx).fill(0).map(() => new Array(ny).fill(0).map(() => new Array(nDir).fill(0)));
let u = new Array(nx).fill(0).map(() => new Array(ny).fill(0).map(() => new Array(2).fill(0)));

// Initial equilibrium value for populations
function initialize() {
    for (let x = 0; x < nx; x++) {
        for (let y = 0; y < ny; y++) {
            let ueq = [uLB, 0];  // Horizontal flow to the right.
            let usqr = ueq[0] * ueq[0] + ueq[1] * ueq[1];
            for (let k = 0; k < nDir; k++) {
                let cu = cx[k] * ueq[0] + cy[k] * ueq[1];
                fin[x][y][k] = rho0 * w[k] * (1 + 3 * cu + 9 * cu * cu / 2 - 3 * usqr / 2);
            }
            u[x][y] = ueq;
        }
    }
}

// Compute post-collision distributions.
function computeCollision(x, y) {
    // 1. Compute macroscopic varibles rho and u
    let rho = 0;
    let ux = 0;
    let uy = 0;

    for (let k = 0; k < nDir; k++) {
        rho += fin[x][y][k];
        // TODO: update ux and uy
    }

    ux /= rho;
    uy /= rho;
    u[x][y][0] = ux;
    u[x][y][1] = uy;

    // 2. Compute post-collision variables fout
    for (let k = 0; k < nDir; k++) {
        const cu = cx[k] * ux + cy[k] * uy;
        const usqr = ux * ux + uy * uy;
        const feq = rho * w[k] * (1 + 3 * cu + 9 * cu * cu / 2 - 3 * usqr / 2);
        // TODO: Implement the collision step
        fout[x][y][k] = ...
    }
}

// Collision step for all nodes
function collide() {
    for (let x = 0; x < nx; x++) {
        for (let y = 0; y < ny; y++) {
            computeCollision(x, y);
        }
    }
}

// Check if the given point is inside the cylinder
function isInsideCylinder(x, y) {
    let dx = x - xCenter;
    let dy = y - yCenter;
    return dx * dx + dy * dy < radius * radius;
}

// Top and bottom wall are going to implement a no-slip condition, like the cylinder
function isWall(x, y) {
    return y < 0 || y >= ny;
}

// Left wall is the inlet
function isInlet(x, y) {
    return x < 0;
}

// Streaming step.
function stream() {
    // Implement outflow condition (copy fout from previous column)
    for (let y = 0; y < ny; y++) {
        for (let k = 0; k < nDir; k++) {
            fout[nx-1][y][k] = fout[nx-2][y][k];
        }
    }

    // Loop over all cells except outflow
    for (let x = 0; x < nx-1; x++) {
        for (let y = 0; y < ny; y++) {
            for (let k = 0; k < nDir; k++) {
                let fromX = x - cx[k];
                let fromY = y - cy[k];
		// Bounce-back on cylinder surface and walls
                if (isInsideCylinder(fromX, fromY) || isWall(fromX, fromY)) {
                }
		// Momentum-exchange bounce-back on inlet
                else if (isInlet(fromX, fromY)) {
                    fin[x][y][k] = fout[x][y][nDir - 1 - k] + 6 * w[k] * uLB;
                }
	        // Normal collision everywhere else
                else {
                    fin[x][y][k] = fout[fromX][fromY][k];
                }
            }
        }
    }
}

// Red-hue colormap
function colormap(value) {
    const colorValue = Math.min(Math.max(Math.floor(value/(1.8 * uLB) * 255), 0), 255);
    return `rgb(${colorValue}, ${colorValue/10}, ${colorValue/10})`;
}

// Rendering the simulation on canvas.
function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  
    for (let x = 0; x < nx; x++) {
        for (let y = 0; y < ny; y++) {
            if (!isInsideCylinder(x, y)) {
                ux = u[x][y][0];
                uy = u[x][y][1];
                ctx.fillStyle = colormap(Math.sqrt(ux * ux + uy * uy));
                ctx.fillRect(x, y, 1, 1);
            }
        }
    }
}

// The main loop.
function mainLoop() {
    for (let step = 0; step < 20; step++) {  // 20 steps per frame.
        collide();
        stream();
    }
  
    draw();
    requestAnimationFrame(mainLoop);
}

// Start the simulation.
initialize();
mainLoop();

